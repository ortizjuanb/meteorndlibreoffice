FROM meteor/node:14.21.4-alpine3.17

RUN apk update

RUN apk --no-cache add \
  bash \
  ca-certificates \
  openjdk8 \
  tzdata \
  libreoffice \
  tesseract-ocr

RUN rm -rf /var/cache/apk/*
